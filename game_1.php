<?php include 'header.php'; ?>
	<div class="container akr-wrapper">
		<div class="row">
			<div class="col-md-8">
				<div class="game-main rounded drop-shadow">
					<div class="action-bar">
						<span class="akr-btn btn-back">Quay lại</span>
					</div>
					
					<div class="box">
						<span class="txt"></span>
						<span class="speaker"></span>
					</div>

					<div class="footer">
						<span class="btn btn-test">Kiểm tra</span>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="game-sidebar rounded drop-shadow">
					<h2 class="title">Kết quả</h2>

					<ul class="question-list">
						<li class="correct">Câu 1</li>
						<li class="wrong">Câu 2</li>
						<li class="active">Câu 3</li>
						<li>Câu 4</li>
						<li>Câu 5</li>
						<li>Câu 6</li>
						<li>Câu 7</li>
						<li>Câu 8</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
<?php include 'footer.php'; ?>